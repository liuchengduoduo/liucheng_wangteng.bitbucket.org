Django==1.9
pyOpenSSL==0.15
tornado==4.1
xhtml2pdf==0.0.6
psutil==3.2.1
gunicorn==19.3.0
psycopg2==2.6
redis==2.10.3
